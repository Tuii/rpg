extends Node

export (String) var slot_name = ""
export (int, FLAGS, "Light", "Medium", "Heavy", "Ranged_One_Slot",\
		"Ranged_Two_Slots") var allowed_types

# Reference for the other slot when using a two handed weapon
export (NodePath) var second_hand

var weapon
