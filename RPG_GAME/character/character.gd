extends Node2D

#enum StatNames {STR, DEX, AGI, CON, SAV}

const DAMAGE_POS = [Vector2(0, -50), Vector2(0, -100)]
const HPBAR_SHOW_TIMER = .25
const MOV_SPEED = 500

export (bool) var is_enemy = false
export (int) var STR = 0
# damage with melee weapons
export (int) var DEX = 0
# hit ratio and damage with ranged weapons and light melee weapons
export (int) var AGI = 0
# turn order, movement speed and dodge ratio
export (int) var CON = 0
# hp
export (int) var SAV = 0
# damage with skills

onready var animation = $Sprite/AnimationPlayer
onready var damage_display = $DamageDisplay
onready var hit_particles = $Sprite/HitParticles
onready var hp_bar = $HPBar
onready var selection = $SelectIndicator
onready var skills = $Skills.get_children()
onready var sprite = $Sprite
onready var timer = $Timer
onready var tw = $Tween
onready var weapon_slots = $WeaponSlots

signal attack_completed
signal died
signal hit_completed
signal move_completed

var attack_range = 1
var attack_target
var coords
var has_attacked
var hp
var level
var is_dead = false
var remaining_speed
var speed

func _ready():
	if is_enemy:
		selection.default_color = Color("ff4632")
	
	tw.start()
	
	hp = CON * 5
	speed = floor(AGI * .5)
	
	hp_bar.max_value = hp
	hp_bar.value = hp


func get_skill(index):
	return skills[index]


func get_skills():
	return skills


func get_weapons():
	var weapons = []
	for slot in weapon_slots.get_children():
		if slot.weapon and !weapons.has(slot.weapon):
			weapons.append(slot.weapon)
	return(weapons)


func flip_sprite(x1, x2):
	if sprite.flip_h != (x1 - x2 < 0):
		sprite.offset *= -1
		sprite.flip_h = !sprite.flip_h
		hit_particles.scale.x = -hit_particles.scale.x


func begin_attack(target):
	attack_target = target
	
	#Turn towards target
	flip_sprite(target.position.x, position.x)
	
	animation.play("attack")
	yield(animation, "animation_finished")
	animation.play("idle")
	emit_signal("attack_completed")


func add_weapon(weapon, weapon_slot):
	var WeaponSlot = get_node(str("WeaponSlots/", weapon_slot))
	var Weapon = load(str("res://weapons/", weapon, "/", weapon, ".tscn")).instance()
	
	assert(WeaponSlot.get_children().empty())
	
	WeaponSlot.add_child(Weapon)
	WeaponSlot.weapon = Weapon


func attack():
	var hit_ratio = DEX
	var dodge_ratio = attack_target.AGI
	
	if rand_range(0, 200) < (168 + hit_ratio) - dodge_ratio:
		attack_target.take_hit(weapon_damage(), self)
	else:
		attack_target.dodge()


func dodge():
	damage_display.text = "Dodge!"
	damage_display.rect_position = DAMAGE_POS[0]
	damage_display.show()
	tw.interpolate_property(damage_display, "rect_position", DAMAGE_POS[0],
		DAMAGE_POS[1], .5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	
	yield(tw, "tween_completed")
	damage_display.hide()
	
	emit_signal("hit_completed")


func take_hit(damage, attacker):
	# Turn towards attacker
	flip_sprite(attacker.position.x, position.x)
	
	# Check death and update HP bar
	hp = max(0, hp - damage)
	hp_bar.show()
	tw.interpolate_property(hp_bar, "value", hp_bar.value, hp, .5,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	
	# Particles
	hit_particles.emitting = true
	
	# Damage text
	damage_display.text = str(damage)
	damage_display.rect_position = DAMAGE_POS[0]
	damage_display.show()
	tw.interpolate_property(damage_display, "rect_position", DAMAGE_POS[0],
		DAMAGE_POS[1], .5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	
	# Hit animation
	if hp > 0:
		animation.play("hit")
		yield(animation, "animation_finished")
		animation.play("idle")
	else:
		die()
	
	yield(tw, "tween_completed")
	damage_display.hide()
	
	# HP bar hide delay
	timer.wait_time = HPBAR_SHOW_TIMER
	timer.start()
	yield(timer, "timeout")
	hp_bar.hide()
	
	emit_signal("hit_completed")


func weapon_damage():
	var wpn = $WeaponSlots.get_children()[0].weapon
	var damage = rand_range(wpn.min_damage, wpn.max_damage)
	if wpn.is_ranged or wpn.type == wpn.LIGHT:
		damage += DEX
	else:
		damage += STR
	
	# Unarmed attack
	return floor(STR * rand_range(.9, 1.1))


func skill_damage(skill):
	return floor(skill.damage * SAV * rand_range(.9, 1.1))


func die():
	is_dead = true
	animation.play("death")
	emit_signal("died", self)
	
	yield(animation, "animation_finished")
	hide()


func move_to(pos, transition, easing):
	flip_sprite(pos.x, position.x)
	var duration = position.distance_to(pos) / MOV_SPEED
	tw.interpolate_property(self, "position", position, pos, duration,
		transition, easing)


func move_along_path(path):
	selection.hide()
	hp_bar.hide()
	animation.play("run")
	remaining_speed -= path.size() - 1
	for i in range(1, path.size()):
		move_to(path[i], Tween.TRANS_LINEAR, Tween.EASE_IN)
		yield(tw, "tween_completed")
	animation.play("idle")
	selection.show()
	hp_bar.show()
	emit_signal("move_completed")


func selected(value):
	if value:
		remaining_speed = speed
		has_attacked = false
	selection.visible = value
	hp_bar.visible = value
