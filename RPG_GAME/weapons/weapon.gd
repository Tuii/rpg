extends Node

enum Type {LIGHT, MEDIUM, HEAVY}
# Light and ranged weapons use DEX for damage
# Medium weapons use STR for damage
# Heavy weapons use STR for damage and take 2 weapon slots

export (Type) var type
export (bool) var is_ranged
export (int) var attack_range
export (int) var min_damage
export (int) var max_damage

func _ready():
	assert (max_damage >= min_damage)
