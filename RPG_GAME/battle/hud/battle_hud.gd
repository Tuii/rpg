extends Control

const BUTTON = preload("res://battle/hud/battle_button.tscn")

onready var buttons = $Buttons.get_children()
onready var attack_button = $Buttons/Attack
onready var cancel_button = $Buttons/Cancel
onready var end_button = $Buttons/End
onready var move_button = $Buttons/Move
onready var skill_button = $Buttons/Skill
onready var skills_menu = $Buttons/SkillsMenu
onready var weapons_menu = $Buttons/WeaponsMenu

signal attack_pressed
signal cancel_pressed
signal end_pressed
signal move_pressed
signal skill_chosen
signal skill_pressed
signal weapon_chosen

func _ready():
	pass


func set_cancel_visibility(value):
	cancel_button.visible = value


func set_menu_visibility(value):
	move_button.visible = value
	attack_button.visible = value
	skill_button.visible = value
	end_button.visible = value


func set_skills_visibility(value):
	skills_menu.visible = (value && skills_menu.get_children().size() > 0)


func set_weapons_visibility(value):
	weapons_menu.visible = value


func show_buttons():
	for b in buttons:
		b.visible = (b != cancel_button)


func show_cancel():
	for b in buttons:
		b.visible = (b == cancel_button)
	skills_menu.visible = true


func set_remaining_movement(value):
	move_button.disabled = (value <= 0)
	var string = str("Mov[[u]e[/u]]")
	var color = Color(.5, .5, .5, .5)
	if value > 0:
		string += str(": ", value)
		color = Color(1, 1, 1)
	move_button.set_label(string, color)


func set_attack(value):
	var color = Color(1, 1, 1) if !value else Color(.5, .5, .5, .5)
	
	attack_button.disabled = value
	attack_button.set_label("[[u]A[/u]]ttack", color)
	
	skill_button.disabled = value
	skill_button.set_label("[[u]S[/u]]kill", color)


func set_skill_buttons(skills):
	var i = 1
	for skill in skills:
		var button = BUTTON.instance()
		skills_menu.add_child(button)
		button.set_label(str("[[u]", i, "[/u]] ", skill.skill_name))
		var sc = load("res://battle/hud/button_shortcuts/shortcut_%d.tres" % i)
		button.shortcut = sc
		button.connect("pressed", self, "_on_Skill_chosen", [i - 1])
		i += 1


func set_weapon_buttons(weapons):
	var i = 1
	for weapon in weapons:
		var button = BUTTON.instance()
		weapons_menu.add_child(button)
		button.set_label(str("[[u]", i, "[/u]] ", weapon.get_name()))
		var sc = load("res://battle/hud/button_shortcuts/shortcut_%d.tres" % i)
		button.shortcut = sc
		button.connect("pressed", self, "_on_Weapon_chosen", [i - 1])
		i += 1


func reset_skill_buttons():
	skills_menu.visible = false
	for child in skills_menu.get_children():
		skills_menu.remove_child(child)
		child.free()


func reset_weapon_buttons():
	weapons_menu.visible = false
	for child in weapons_menu.get_children():
		weapons_menu.remove_child(child)
		child.free()


func _on_Attack_pressed():
	if weapons_menu.get_children().size() > 1:
		set_menu_visibility(false)
		set_cancel_visibility(true)
		set_weapons_visibility(true)
		emit_signal("attack_pressed")
	else:
		emit_signal("weapon_chosen", 0)


func _on_Weapon_chosen(index):
	emit_signal("weapon_chosen", index)


func _on_Cancel_pressed():
	emit_signal("cancel_pressed")


func _on_End_pressed():
	emit_signal("end_pressed")


func _on_Move_pressed():
	emit_signal("move_pressed")


func _on_Skill_pressed():
	emit_signal("skill_pressed")


func _on_Skill_chosen(index):
	emit_signal("skill_chosen", index)
