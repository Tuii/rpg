extends Button

onready var label = $RichTextLabel

func _ready():
	pass


func set_label(text, color=Color(1, 1, 1)):
	label.bbcode_text = text
	label.modulate = color
