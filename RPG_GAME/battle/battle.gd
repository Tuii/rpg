extends Node

const COLOR_ATK = Color(1, .5, .5, .7)
const COLOR_MOV = Color(.5, .7, 1, .8)

enum States {ATTACK, IDLE, MOVEMENT, SKILL_MENU, WEAPON_MENU, SKILL_READY, BATTLE_ENDED}

onready var characters = $Characters.get_children()
onready var grid = $Grid
onready var hud = $BattleHUD

var char_at = {}
var chosen_skill
var chosen_weapon
var current
var state = IDLE

func _ready():
	for i in range(characters.size()):
		var cell = grid.get_used_cells()[i]
		char_at[cell] = characters[i]
		characters[i].coords = cell
		characters[i].position = grid.hex_center(cell)
		characters[i].connect("died", self, "_on_character_death")
		grid.set_cellv(cell,
			grid.ENEMY if characters[i].is_enemy else grid.FRIENDLY)
		
		#TEST
		if i < 2:
			characters[i].add_weapon("dagger", "Snout")
		else:
			characters[i].add_weapon("club", "LeftPaw")
			characters[i].add_weapon("club", "RightPaw")
	
	current = -1
	next_char()


func next_char():
	characters[current].selected(false)
	current = (current + 1) % characters.size()
	if characters[current].is_dead:
		next_char()
		return
	characters[current].selected(true)
	hud.reset_skill_buttons()
	hud.set_skill_buttons(characters[current].get_skills())
	hud.reset_weapon_buttons()
	hud.set_weapon_buttons(characters[current].get_weapons())
	change_state(IDLE)


func _on_character_death(character):
	grid.set_cellv(character.coords, grid.EMPTY)
	char_at.erase(character.coords)
	
	if check_battle_end():
		print("ended")
		change_state(BATTLE_ENDED)


func change_state(new_state):
	if state == BATTLE_ENDED:
		return
	
	state = new_state
	match state:
		ATTACK:
			set_attack_buttons(characters[current])
			hud.set_menu_visibility(false)
			hud.set_cancel_visibility(true)
			hud.set_weapons_visibility(false)
		WEAPON_MENU:
			hud.set_menu_visibility(false)
			hud.set_cancel_visibility(true)
			hud.set_weapons_visibility(true)
		IDLE:
			hud.set_remaining_movement(characters[current].remaining_speed)
			hud.set_attack(characters[current].has_attacked)
			hud.set_menu_visibility(true)
			hud.set_cancel_visibility(false)
			hud.set_skills_visibility(false)
			hud.set_weapons_visibility(false)
		MOVEMENT:
			set_movement_buttons(characters[current])
			hud.set_menu_visibility(false)
			hud.set_cancel_visibility(true)
		SKILL_MENU:
			hud.set_menu_visibility(false)
			hud.set_cancel_visibility(true)
			hud.set_skills_visibility(true)
		SKILL_READY:
			set_skill_buttons(chosen_skill)
			grid.begin_aiming_skill(chosen_skill)
			hud.set_skills_visibility(false)
		BATTLE_ENDED:
			hud.set_menu_visibility(false)
			hud.set_cancel_visibility(false)
			hud.set_skills_visibility(false)
			hud.set_weapons_visibility(false)
			grid.disable_all()


func cancel_state():
	match(state):
		ATTACK:
			grid.disable_all()
			change_state(IDLE)
		WEAPON_MENU:
			change_state(IDLE)
		MOVEMENT:
			grid.disable_all()
			grid.end_pathing()
			change_state(IDLE)
		SKILL_MENU:
			change_state(IDLE)
		SKILL_READY:
			grid.end_aiming_skill()
			change_state(SKILL_MENU)


func set_attack_buttons(character):
	var within_range = grid_functions.get_positions_in_range(character.coords,
		character.attack_range)
	within_range.pop_front()
	var enemies = []
	var enemy_value = grid.FRIENDLY if character.is_enemy else grid.ENEMY 
	
	for cell in within_range:
		if grid.get_cellv(cell) == enemy_value:
			enemies.append(cell)
	
	grid.enable_buttons(enemies)
	grid.color_buttons(within_range, COLOR_ATK)


func set_movement_buttons(character):
	var char_v = grid.get_cellv(character.coords)
	var to_check = [character.coords]
	var checked = []
	var to_remove = []
	
	for i in range(character.remaining_speed + 1):
		for j in range(to_check.size()):
			
			var cell = to_check.pop_front()
			checked.append(cell)
			
			if i >= character.remaining_speed:
				continue
			
			for adj in grid.get_adjacent_cells(cell):
				if to_check.has(adj) or checked.has(adj):
					continue
				var adj_v = grid.get_cellv(adj)
				if adj_v == grid.EMPTY or adj_v == char_v or (char_at.has(adj)\
				and char_at[adj] and char_at[adj].is_dead):
					to_check.append(adj)
					if adj_v != grid.EMPTY:
						to_remove.append(adj)
				else:
					grid.remove_cell_from_path(adj)
	
	for cell in to_remove:
		checked.erase(cell)
	
	grid.enable_buttons(checked)
	grid.color_buttons(checked, COLOR_MOV)
	grid.begin_pathing(character.coords)


func set_skill_buttons(skill):
	skill.origin_pos = characters[current].coords
	var skill_area = skill.get_selectable_area()
	grid.enable_buttons(skill_area)
	grid.color_buttons(skill_area, COLOR_ATK)


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		cancel_state()


func _on_Grid_cell_selected(cell):
	match state:
		MOVEMENT:
			var ch = characters[current]
			if (cell != ch.coords):
				hud.visible = false
				
				char_at.erase(ch.coords)
				char_at[cell] = ch
				
				var path = grid.get_path()
				if path.size() <= 0:
					return
				
				grid.disable_all()
				grid.end_pathing()
				grid.set_cellv(ch.coords, grid.EMPTY)
				var value = grid.ENEMY if ch.is_enemy else grid.FRIENDLY
				grid.set_cellv(cell, value)
				ch.move_along_path(path)
				
				yield(ch, "move_completed")
				ch.coords = cell
				hud.visible = true
			else:
				grid.disable_all()
			change_state(IDLE)
		ATTACK:
			hud.visible = false
			grid.disable_all()
			
			var attacker = characters[current]
			var defender = char_at[cell]
			
			attacker.has_attacked = true
			attacker.begin_attack(defender)
			
			yield(defender, "hit_completed")
			hud.visible = true
			change_state(IDLE)
		SKILL_READY:
			characters[current].has_attacked = true
			for pos in chosen_skill.target_area:
				var val = grid.get_cellv(pos)
				var dmg = characters[current].skill_damage(chosen_skill)
				if val == grid.FRIENDLY or val == grid.ENEMY:
					char_at[pos].take_hit(dmg, characters[current])
			grid.end_aiming_skill()
			if characters[current].is_dead:
				next_char()
			else:
				change_state(IDLE)


func _on_BattleHUD_attack_pressed():
	change_state(WEAPON_MENU)


func _on_BattleHUD_weapon_chosen(index):
	chosen_weapon = characters[current].get_weapons()[index]
	change_state(ATTACK)


func _on_BattleHUD_cancel_pressed():
	cancel_state()


func _on_BattleHUD_end_pressed():
	next_char()


func _on_BattleHUD_move_pressed():
	change_state(MOVEMENT)


func _on_BattleHUD_skill_pressed():
	change_state(SKILL_MENU)


func _on_BattleHUD_skill_chosen(index):
	chosen_skill = characters[current].get_skill(index)
	change_state(SKILL_READY)


func get_all_enemies():
	var enemies = []
	for chara in characters:
		if chara.is_enemy:
			enemies.append(chara)
	return enemies


func get_all_friendlies():
	var friendlies = []
	for chara in characters:
		if not chara.is_enemy:
			friendlies.append(chara)
	return friendlies


# If char_array is sent, will check for living characters in the char_array only
func get_living_characters(char_array):
	var units = characters
	if char_array:
		units = char_array
	
	var living = []
	for c in char_array:
		if c.hp > 0:
			living.append(c)
	
	return living


func check_battle_end():
	var living_enemies = get_living_characters(get_all_enemies())
	var living_friendlies = get_living_characters(get_all_friendlies())
	
	if living_enemies.empty():
		return "win"
	if living_friendlies.empty():
		return "lose"
	return null
