extends TextureButton

onready var solid_color = $SolidColor

var coords

func set_color(color, solid=false):
	solid_color.visible = (solid and color.a > 0)
	if solid:
		solid_color.modulate = color
	else:
		self_modulate = color