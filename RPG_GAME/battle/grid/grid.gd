extends TileMap

const COLOR_DISABLED = Color(1, 1, 1, .3)

enum CellState {EMPTY, FRIENDLY, ENEMY, OBSTACLE}

onready var buttons = $Buttons
onready var path = $Path
onready var path_head = $Path/Head
onready var pathfinder = AStar.new()

var aiming_skill = null
var btn_dict = {}
var enabled_buttons = []
var id = {}
var origin = Vector2()
var pathing = false
var removed_from_path = []

signal cell_selected(c)

var grid_button = preload("res://battle/grid/grid_button.tscn")

func _ready():
	var i = 1
	for cell in get_used_cells():
		# Instancing buttons
		var btn = grid_button.instance()
		btn.coords = cell
		btn.rect_position = map_to_world(cell)
		btn.connect("pressed", self, "button_pressed", [btn])
		btn.connect("mouse_entered", self, "button_entered", [btn])
		btn.connect("mouse_exited", self, "button_exited", [btn])
		buttons.add_child(btn)
		btn_dict[cell] = btn
		btn.set_color(COLOR_DISABLED)
		
		# Creating AStar
		id[cell] = i
		i += 1
		var pos = hex_center(cell)
		pathfinder.add_point(id[cell], Vector3(pos.x, pos.y, 0))
		for adj in get_adjacent_cells(cell):
			if id.has(adj) and pathfinder.has_point(id[adj]):
				pathfinder.connect_points(id[cell], id[adj])


func button_pressed(btn):
	if btn in enabled_buttons:
		emit_signal("cell_selected", btn.coords)


func button_entered(btn):
	if pathing and btn in enabled_buttons:
		for p in pathfinder.get_point_path(id[origin], id[btn.coords]):
			path.add_point(Vector2(p.x, p.y))
		
		if path.points.size() > 1:
			path_head.show()
		
		var point_count = path.get_point_count()
		var last = path.get_point_position(point_count - 1)
		path_head.position = last
		if path.get_point_count() > 1:
			var seclast = path.get_point_position(point_count - 2)
			path_head.rotation = last.angle_to_point(seclast)
	elif aiming_skill:
		var color_area = []
		if btn.coords in aiming_skill.selectable_area:
			color_area = aiming_skill.get_target_area(btn.coords)
		
		for pos in color_area:
			if btn_dict.has(pos):
				btn_dict[pos].set_color(Color(1, .5, .5, .7), true)


func button_exited(btn):
	if pathing:
		path.points = []
		path_head.hide()
	elif aiming_skill:
		for btn in btn_dict.values():
			btn.set_color(Color(1, 1, 1, 0), true)


func begin_aiming_skill(skill):
	aiming_skill = skill

	for b in btn_dict.values():
		if b.is_hovered():
			button_entered(b)
			return


func end_aiming_skill():
	aiming_skill = null
	disable_all()


func begin_pathing(path_origin):
	pathing = true
	origin = path_origin
	
	for b in btn_dict.values():
		if b.is_hovered():
			button_entered(b)
			return


func end_pathing():
	pathing = false
	path_head.hide()
	path.points = []
	while removed_from_path.size():
		var cell = removed_from_path.pop_front()
		var pos = hex_center(cell)
		pathfinder.add_point(id[cell], Vector3(pos.x, pos.y, 0))
		for adj in get_adjacent_cells(cell):
			if id.has(adj) and pathfinder.has_point(id[adj]) and \
				!pathfinder.are_points_connected(id[cell], id[adj]):
				pathfinder.connect_points(id[cell], id[adj])


func get_path():
	return path.points


func remove_cell_from_path(cell):
	if pathfinder.has_point(id[cell]):
		pathfinder.remove_point(id[cell])
	removed_from_path.append(cell)


func hex_center(cell):
	return (map_to_world(cell) + Vector2(cell_size.x * 2 / 3, cell_size.y / 2))


func get_adjacent_cells(cell):
	var positions = grid_functions.get_adjacent_positions(cell)
	
	var cells = []
	for p in positions:
		if get_cellv(p) != INVALID_CELL:
			cells.append(p)
	
	return cells


func enable_buttons(coords):
	for btn in buttons.get_children():
		if btn.coords in coords:
			enabled_buttons.append(btn)


func disable_all():
	enabled_buttons = []
	reset_button_colors()


func color_buttons(coords, color):
	for btn in buttons.get_children():
		if btn.coords in coords:
			btn.set_color(color)


func reset_button_colors():
	for btn in buttons.get_children():
		btn.set_color(COLOR_DISABLED)
