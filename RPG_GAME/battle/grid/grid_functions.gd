extends Node

enum Directions {UP, UR, DR, DW, DL, UL}

func get_adjacent_position(pos, dir):
	match dir:
		UP:
			return Vector2(pos.x, pos.y - 1)
		UR:
			return Vector2(pos.x + 1, pos.y - 1 + int(pos.x) % 2)
		DR:
			return Vector2(pos.x + 1, pos.y + int(pos.x) % 2)
		DW:
			return Vector2(pos.x, pos.y + 1)
		DL:
			return Vector2(pos.x - 1, pos.y + int(pos.x) % 2)
		UL:
			return Vector2(pos.x - 1, pos.y - 1 + int(pos.x) % 2)


func get_adjacent_positions(pos):
	var adjacent = []
	
	for dir in Directions.values():
		adjacent.append(get_adjacent_position(pos, dir))
	
	return adjacent


func get_direction_between(orig, targ):
	assert(orig.x != targ.x or orig.y != targ.y)
	
	if orig.x == targ.x:
		if orig.y > targ.y:
			return UP
		else:
			return DW
	
	if int(orig.x) % 2:
		if orig.x > targ.x:
			if orig.y >= targ.y:
				return UL
			else:
				return DL
		else:
			if orig.y >= targ.y:
				return UR
			else:
				return DR
	else:
		if orig.x > targ.x:
			if orig.y > targ.y:
				return UL
			else:
				return DL
		else:
			if orig.y > targ.y:
				return UR
			else:
				return DR


func get_positions_in_range(pos, ran):
	var positions = [pos]
	var border = get_adjacent_positions(pos)
	
	for i in range(ran):
		for j in range(border.size()):
			var b = border.pop_front()
			positions.append(b)
			
			if i == ran - 1:
				continue
			
			for a in get_adjacent_positions(b):
				if !positions.has(a) and !border.has(a):
					border.append(a)
	
	return positions
