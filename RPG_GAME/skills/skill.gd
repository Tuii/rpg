extends Node

enum Shapes {ARC, BLOB, LINE}

export (String) var skill_name = ""
export (int) var damage = 0
export (int) var blob_range = 0
export (int) var blob_radius = 0
export (int) var size = 1
export (Shapes) var shape = 0

var selectable_area
var target_area
var origin_pos = Vector2()

func _ready():
	validate_size()


func validate_size():
	assert(size > 0)
	match shape:
		ARC:
			assert(size <= 6)


func get_selectable_area():
	match shape:
		ARC:
			selectable_area = arc_selectable_area()
		BLOB:
			selectable_area = blob_selectable_area()
		LINE:
			selectable_area = line_selectable_area()
	
	return selectable_area


func get_target_area(target_pos):
	match shape:
		ARC:
			target_area = arc_target_area(target_pos)
		BLOB:
			target_area = blob_target_area(target_pos)
		LINE:
			target_area = line_target_area(target_pos)
	
	return target_area


func arc_selectable_area():
	return grid_functions.get_adjacent_positions(origin_pos)


func blob_selectable_area():
	return grid_functions.get_positions_in_range(origin_pos, blob_range)


func line_selectable_area():
	var area = []
	
	for dir in grid_functions.Directions.values():
		var pos = origin_pos
		for i in range(size):
			pos = grid_functions.get_adjacent_position(pos, dir)
			area.append(pos)
	
	return area


func arc_target_area(target_pos):
	var area = selectable_area
	if size != 6:
		area = [target_pos]
		var side = -1
		var dir = grid_functions.get_direction_between(origin_pos, target_pos)
		for i in range(size - 1):
			side *= -1
			var dir2 = dir + side * ((i / 2) + 1)
			dir2 = wrapi(dir2, 0, grid_functions.Directions.size())
			area.append(grid_functions.get_adjacent_position(origin_pos, dir2))
	
	return area


func blob_target_area(target_pos):
	return grid_functions.get_positions_in_range(target_pos, blob_radius)


func line_target_area(target_pos):
	var area = []
	var direction = grid_functions.get_direction_between(origin_pos, target_pos)
	var pos = origin_pos
	
	for i in range(size):
		pos = grid_functions.get_adjacent_position(pos, direction)
		area.append(pos)
	
	return area
